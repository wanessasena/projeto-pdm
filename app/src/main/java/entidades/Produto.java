package entidades;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName="produto",
        foreignKeys = {
            @ForeignKey(entity = Categoria.class, parentColumns = "id", childColumns = "categoria_id"),
                @ForeignKey(entity = Loja.class, parentColumns = "id", childColumns = "loja_id"),
                @ForeignKey(entity = Imagem.class, parentColumns = "id", childColumns = "imagem_id")
        }, indices = {@Index("categoria_id"), @Index("loja_id"), @Index("imagem_id")})
//@Entity(tableName = "produto", indices = {@Index("categoria_id"), @Index("loja_id"), @Index("imagem_id")})
public class Produto implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "nome")
    private String nome;

    @ColumnInfo(name = "preco_original")
    private double preco_original;

    @ColumnInfo(name = "preco_atual")
    private double preco_atual;

    @ColumnInfo(name = "categoria_id")
    private long categoria_id;

    @ColumnInfo(name = "loja_id")
    private long loja_id;

    @ColumnInfo(name = "imagem_id")
    private long imagem_id;

    @ColumnInfo(name = "latitude")
    private double latitude;

    @ColumnInfo(name = "longitude")
    private double longitude;

    public Produto(String nome, double preco_original, double preco_atual, long categoria_id, long loja_id, long imagem_id, double latitude, double longitude) {
        this.nome = nome;
        this.preco_original = preco_original;
        this.preco_atual = preco_atual;
        this.categoria_id = categoria_id;
        this.loja_id = loja_id;
        this.imagem_id = imagem_id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco_original() {
        return preco_original;
    }

    public void setPreco_original(double preco_original) {
        this.preco_original = preco_original;
    }

    public double getPreco_atual() {
        return preco_atual;
    }

    public void setPreco_atual(double preco_atual) {
        this.preco_atual = preco_atual;
    }

    public long getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(long categoria_id) {
        this.categoria_id = categoria_id;
    }

    public long getLoja_id() {
        return loja_id;
    }

    public void setLoja_id(long loja_id) {
        this.loja_id = loja_id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getImagem_id() {
        return imagem_id;
    }

    public void setImagem_id(long imagem_id) {
        this.imagem_id = imagem_id;
    }

    @Override
    public String toString() {
        return    "Id: " + id + "\n" +
                            "Nome: " + nome + "\n" +
                            "Preço original:" + preco_original + "\n" +
                            "Preco atual: " + preco_atual + "\n" +
                            "CategoriaId: " + categoria_id + "\n" +
                            "LojaId:" + loja_id + "\n" +
                            "ImagemId:" + imagem_id;
    }
}
