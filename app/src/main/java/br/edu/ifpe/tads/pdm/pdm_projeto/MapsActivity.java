package br.edu.ifpe.tads.pdm.pdm_projeto;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

import adapter.ProductArrayListAdapter;
import dao.CategoriaDao;
import db.Database;
import entidades.Categoria;
import entidades.Imagem;
import entidades.Loja;
import entidades.Produto;
import estruturas.ProdutoCompleto;
import singleton.SingletonProdutoCompleto;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback{

    private static final int FINE_LOCATION_REQUEST = 0;
    private boolean fine_location;
    private GoogleMap mMap;
    private ListView listView;
    private Database db;
    public List<ProdutoCompleto> produtosCompletos;
    private Spinner spinner_categorias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        requestPermission();

        db = Database.getInstance(getApplicationContext());
        listView = (ListView) findViewById(R.id.listView);
        produtosCompletos = new ArrayList<>();
        final EditText editText = (EditText) findViewById(R.id.input_search);
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Produto> produtos = null;
                do {
                    produtos = db.produtoDao().getAll();
                }while(produtos.size() == 0);
                Log.i("Tamanho: ", produtos.size() + "");
                for(Produto produto : produtos) {
                    Categoria c = db.categoriaDao().getCategoriaById(produto.getCategoria_id());
                    Loja l = db.lojaDao().getLojaById(produto.getLoja_id());
                    Imagem i = db.imagemDao().getImagemById(produto.getImagem_id());
                    produtosCompletos.add(new ProdutoCompleto(produto, c.getNome(), l.getNome(), i.getBinario()));
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final ProductArrayListAdapter adapter = new ProductArrayListAdapter(getApplicationContext(), R.layout.products_list, produtosCompletos);
                        listView.setAdapter(adapter);
                        addCategoriesSpinner();
                        editText.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before,
                                                      int count) {
                                //quando o texto é alterado chamamos o filtro.
                                adapter.getFilter().filter(s.toString());
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count,
                                                          int after) {
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                            }
                        });

                        spinner_categorias = (Spinner) findViewById(R.id.spinner_categories);
                        spinner_categorias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                String cat_selec = spinner_categorias.getSelectedItem().toString();
                                adapter.getFilterCategorias().filter(cat_selec);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }

                        });
                    }
                });
            }
        }).start();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
                ProdutoCompleto produto = (ProdutoCompleto) adapter.getItemAtPosition(position);
                Intent intent = new Intent(view.getContext(), DetailActivity.class);
                SingletonProdutoCompleto singleton = SingletonProdutoCompleto.getInstance();
                singleton.setProduto(produto);
                startActivity(intent);
            }
        });

    }

    private void requestPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        boolean hasPermission = (permissionCheck == PackageManager.PERMISSION_GRANTED);

        if (hasPermission) return;
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_REQUEST);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fine_location = true;
            mMap.setMyLocationEnabled(fine_location);
            setCurrentLocation();
            setProductsLocation();
        }
    }

    @SuppressLint("MissingPermission")
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        boolean granted = (grantResults.length > 0) &&
                (grantResults[0] == PackageManager.PERMISSION_GRANTED);
        fine_location = (requestCode == FINE_LOCATION_REQUEST) && granted;

        mMap.setMyLocationEnabled(fine_location);

        setCurrentLocation();
    }

    public void setCurrentLocation() {
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        @SuppressLint("MissingPermission") Task<Location> task = fusedLocationProviderClient.getLastLocation();

        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location!=null) {
                    LatLng currLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(currLocation).title("Você está aqui").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(currLocation));
                }
            }
        });
    }

    public void setProductsLocation(){
        while(produtosCompletos.size() == 0);
        for(int i = 0; i < produtosCompletos.size(); i++){
            LatLng currLocation = new LatLng(produtosCompletos.get(i).getLatitude(), produtosCompletos.get(i).getLongitude());
            mMap.addMarker(new MarkerOptions().position(currLocation).title(produtosCompletos.get(i).getNome()));
        }
    }

    public void addCategoriesSpinner(){
        spinner_categorias = (Spinner) findViewById(R.id.spinner_categories);

        db = Database.getInstance(getApplicationContext());
        listView = (ListView) findViewById(R.id.listView);
        produtosCompletos = new ArrayList<>();


        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Categoria> getAllCategories = null;
                final List<String> categorias = new ArrayList<String>();
                categorias.add("Todas as categorias");
                do {
                    getAllCategories = db.categoriaDao().getAll();
                }while(getAllCategories.size() == 0);
                for(Categoria categoria : getAllCategories) {
                    categorias.add(categoria.getNome());
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                android.R.layout.simple_spinner_item, categorias);

                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_categorias.setAdapter(dataAdapter);
                    }
                });
            }
        }).start();

    }
}
