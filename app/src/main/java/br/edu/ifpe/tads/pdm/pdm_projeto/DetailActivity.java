package br.edu.ifpe.tads.pdm.pdm_projeto;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import singleton.SingletonProdutoCompleto;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private boolean fine_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Detalhes do produto");

        SingletonProdutoCompleto singleton = SingletonProdutoCompleto.getInstance();

        ImageView productImg = (ImageView) findViewById(R.id.product_Image);
        TextView productCategory = (TextView) findViewById(R.id.product_Category);
        TextView productName = (TextView) findViewById(R.id.product_Name);
        TextView productDiscount = (TextView) findViewById(R.id.product_Discount);
        TextView productDescription = (TextView) findViewById(R.id.product_Description);
        TextView productOlderPrice = (TextView) findViewById(R.id.product_OlderPrice);
        TextView productNewPrice = (TextView) findViewById(R.id.product_NewPrice);

        productImg.setImageResource(singleton.getProduto().getImagem());
        productCategory.setText(singleton.getProduto().getCategoria());
        productName.setText(singleton.getProduto().getNome());

        String discount = "-" + Double.toString(Math.round((singleton.getProduto().getPreco_original() - singleton.getProduto().getPreco_atual())/singleton.getProduto().getPreco_original()*100)) + " %";
        productDiscount.setText(discount);
        productOlderPrice.setText(String.format("R$ %.2f",singleton.getProduto().getPreco_original()));
        productNewPrice.setText(String.format("R$ %.2f",singleton.getProduto().getPreco_atual()));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fine_location = true;
            mMap.setMyLocationEnabled(fine_location);

            SingletonProdutoCompleto singleton = SingletonProdutoCompleto.getInstance();

            LatLng currLocation = new LatLng(singleton.getProduto().getLatitude(), singleton.getProduto().getLongitude());
            mMap.addMarker(new MarkerOptions().position(currLocation).title(singleton.getProduto().getLoja()));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(currLocation));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Botão adicional na ToolBar
        switch (item.getItemId()) {
            case android.R.id.home:  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
                startActivity(new Intent(this, MapsActivity.class));  //O efeito ao ser pressionado do botão (no caso abre a activity)
                finishAffinity();  //Método para matar a activity e não deixa-lá indexada na pilhagem
                break;
            default:break;
        }
        return true;
    }
}
