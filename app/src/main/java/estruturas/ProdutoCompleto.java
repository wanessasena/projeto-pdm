package estruturas;

import androidx.room.ColumnInfo;

import entidades.Produto;

public class ProdutoCompleto {

    private long id;

    private String nome;

    private double preco_original;

    private double preco_atual;

    private String  categoria;

    private String loja;

    private int imagem;

    private double latitude;

    private double longitude;

    public ProdutoCompleto(Produto produto, String categoria, String loja, int imagem) {

        this.id = produto.getId();
        this.nome = produto.getNome();
        this.preco_original = produto.getPreco_original();
        this.preco_atual = produto.getPreco_atual();
        this.categoria = categoria;
        this.loja = loja;
        this.imagem = imagem;
        this.latitude = produto.getLatitude();
        this.longitude = produto.getLongitude();
    }

    public ProdutoCompleto() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco_original() {
        return preco_original;
    }

    public void setPreco_original(double preco_original) {
        this.preco_original = preco_original;
    }

    public double getPreco_atual() {
        return preco_atual;
    }

    public void setPreco_atual(double preco_atual) {
        this.preco_atual = preco_atual;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getLoja() {
        return loja;
    }

    public void setLoja(String loja) {
        this.loja = loja;
    }

    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
