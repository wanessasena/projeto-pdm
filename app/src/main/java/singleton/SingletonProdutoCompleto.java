package singleton;

import estruturas.ProdutoCompleto;

public class SingletonProdutoCompleto {
    private static SingletonProdutoCompleto instance;

    private ProdutoCompleto produto;

    public static SingletonProdutoCompleto getInstance() {
        if(instance == null){
            instance = new SingletonProdutoCompleto();
        }
        return instance;
    }

    private SingletonProdutoCompleto(){
        this.produto = new ProdutoCompleto();
    }

    public ProdutoCompleto getProduto() {
        return produto;
    }

    public void setProduto(ProdutoCompleto produto) {
        this.produto = produto;
    }
}
