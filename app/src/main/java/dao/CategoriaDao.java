package dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import entidades.Categoria;

@Dao
public interface CategoriaDao {

    @Query("SELECT * FROM categoria")
    List<Categoria> getAll();

    @Query("SELECT * FROM categoria WHERE id = :id LIMIT 1")
    Categoria getCategoriaById(long id);

    @Query("SELECT * FROM categoria WHERE nome LIKE :nome LIMIT 1")
    Categoria getCategoriaByNome(String nome);

    @Insert
    long inserir(Categoria categoria);

    @Insert
    void inserirLista(List<Categoria> categorias);
}
