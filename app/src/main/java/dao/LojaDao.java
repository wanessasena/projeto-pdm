package dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import entidades.Loja;

@Dao
public interface LojaDao {

    @Query("SELECT * FROM loja")
    List<Loja> getAll();

    @Query("SELECT * FROM loja WHERE id = :id LIMIT 1")
    Loja getLojaById(long id);

    @Query("SELECT * FROM loja WHERE nome LIKE :nome LIMIT 1")
    Loja getLojaByNome(String nome);

    @Insert
    void inserirTudo(List<Loja> lojas);
}
