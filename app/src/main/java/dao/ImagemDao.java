package dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import entidades.Imagem;

@Dao
public interface ImagemDao {

    @Query("SELECT * FROM imagem WHERE id = :id LIMIT 1")
    Imagem getImagemById(long id);

    @Query("SELECT * FROM imagem WHERE nome_arquivo LIKE :nome LIMIT 1")
    Imagem getImagemByNome(String nome);

    @Insert
    void inserirLista(List<Imagem> imagens);
}
