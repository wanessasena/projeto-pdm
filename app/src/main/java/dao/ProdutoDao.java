package dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import entidades.Produto;

@Dao
public interface ProdutoDao {

    @Query("SELECT * FROM produto")
    List<Produto> getAll();

    @Query("SELECT * FROM produto WHERE loja_id = :idLoja")
    List<Produto> getAllByLojaId(long idLoja);

    @Query("SELECT * FROM produto WHERE id = :id")
    Produto getProdutoById(long id);

    @Insert
    void inserirTudo(List<Produto> produtos);
}
