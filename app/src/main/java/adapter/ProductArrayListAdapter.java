package adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import br.edu.ifpe.tads.pdm.pdm_projeto.R;
import entidades.Produto;
import estruturas.ProdutoCompleto;

public class ProductArrayListAdapter extends ArrayAdapter<ProdutoCompleto> {
    private List<ProdutoCompleto> produtos;
    private List<ProdutoCompleto> prodFiltrado;

    public ProductArrayListAdapter(Context context, int resource, List<ProdutoCompleto> produtos){
        super(context, resource, produtos);
        this.produtos = produtos;
        this.prodFiltrado = produtos;
    }

    @Override
    public int getCount() {
        if(prodFiltrado != null){
            return prodFiltrado.size();
        }
        return 0;
    }

    @Override
    public ProdutoCompleto getItem(int index) {
        if(prodFiltrado != null){
            return prodFiltrado.get(index);
        }
        return null;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        view = inflater.inflate(R.layout.products_list, null, true);

        ImageView productImg = view.findViewById(R.id.productImg);
        TextView productTitle = view.findViewById(R.id.productTitle);
        TextView productCategory = view.findViewById(R.id.productCategory);
        TextView productStore = view.findViewById(R.id.productStore);
        TextView productOlderPrice = view.findViewById(R.id.productOlderPrice);
        TextView productNewPrice = view.findViewById(R.id.productNewPrice);

        productImg.setImageResource(prodFiltrado.get(position).getImagem());
        productTitle.setText(prodFiltrado.get(position).getNome());
        productCategory.setText("Categoria: " + prodFiltrado.get(position).getCategoria());
        productStore.setText("Loja: " + prodFiltrado.get(position).getLoja());
        productOlderPrice.setText(String.format("R$ %.2f",prodFiltrado.get(position).getPreco_original()));
        productNewPrice.setText(String.format("R$ %.2f",prodFiltrado.get(position).getPreco_atual()));

        return view;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence filtro) {
                FilterResults resultados = new FilterResults();
                if(filtro == null || filtro.length() == 0) {
                    resultados.count = getCount();
                    resultados.values = produtos;
                    prodFiltrado = produtos;
                }else {
                    List<ProdutoCompleto> produtosFiltrados = new ArrayList<>();
                    for(int i = 0; i < produtos.size(); i++) {
                        ProdutoCompleto prod = produtos.get(i);
                        filtro = filtro.toString().toLowerCase();
                        String condicaoNome = prod.getNome().toLowerCase();
                        String condicaoLoja = prod.getLoja().toLowerCase();
                        if(condicaoNome.contains(filtro) || condicaoLoja.contains(filtro))
                            produtosFiltrados.add(prod);
                    }
                    resultados.count = produtosFiltrados.size();
                    resultados.values = produtosFiltrados;
                    prodFiltrado = produtosFiltrados;
                }
                return resultados;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                prodFiltrado = (List<ProdutoCompleto>) filterResults.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    public Filter getFilterCategorias(){
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence filtro) {
                FilterResults resultados = new FilterResults();
                if(filtro == null || filtro.length() == 0 || filtro == "Todas as categorias") {
                    resultados.count = getCount();
                    resultados.values = produtos;
                    prodFiltrado = produtos;
                }else {
                    List<ProdutoCompleto> produtosFiltrados = new ArrayList<>();
                    for(int i = 0; i < produtos.size(); i++) {
                        ProdutoCompleto prod = produtos.get(i);
                        filtro = filtro.toString().toLowerCase();
                        String condicaoCategoria = prod.getCategoria().toLowerCase();
                        if(condicaoCategoria.contains(filtro))
                            produtosFiltrados.add(prod);
                    }
                    resultados.count = produtosFiltrados.size();
                    resultados.values = produtosFiltrados;
                    prodFiltrado = produtosFiltrados;
                }
                return resultados;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                prodFiltrado = (List<ProdutoCompleto>) filterResults.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }
}
