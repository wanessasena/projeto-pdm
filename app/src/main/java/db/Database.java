package db;



import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import br.edu.ifpe.tads.pdm.pdm_projeto.R;
import dao.CategoriaDao;
import dao.ImagemDao;
import dao.LojaDao;
import dao.ProdutoDao;
import entidades.Categoria;
import entidades.Imagem;
import entidades.Loja;
import entidades.Produto;

@androidx.room.Database(entities = {Categoria.class, Imagem.class, Loja.class, Produto.class}, version = 1, exportSchema = false)
public abstract class Database extends RoomDatabase {

    private static Database db;

    public abstract CategoriaDao categoriaDao();
    public abstract ImagemDao imagemDao();
    public abstract LojaDao lojaDao();
    public abstract ProdutoDao produtoDao();

    public synchronized static Database getInstance(Context context) {
        if(db == null) {
            db = buildDatabase(context);
        }
        return db;
    }

    private static Database buildDatabase(final Context context) {
        Log.i("Build: ", "yes");
        return Room.databaseBuilder(context, Database.class, "bancoteste4").addCallback(new Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);
                Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("Create: ", "yes");
                        Database db = getInstance(context);
                        List<Categoria> categorias = new ArrayList<>();
                        String[] nomesCategoria = {"Livros", "Computadores", "Vestuário", "Serviços", "Jogos"};
                        for(String nome : nomesCategoria) {
                            categorias.add(new Categoria(nome));
                        }
                        db.categoriaDao().inserirLista(categorias);
                        Log.i("Insert:", "categorias");
                        List<Loja> lojas = new ArrayList<>();
                        String[] nomeLoja = {"Saraiva", "C&A", "Nagem", "Game Station"};
                        for(String nome : nomeLoja) {
                            lojas.add(new Loja(nome));
                        }
                        db.lojaDao().inserirTudo(lojas);
                        Log.i("Insert:", "lojas");
                        List<Imagem> imagens = new ArrayList<>();
                        String[] nomeImagem = {"Livro.jpg", "Computador.jpg", "Camisa.jpg", "Jogo.jpg"};

                        imagens.add(new Imagem(nomeImagem[0], R.drawable.livro));

                        imagens.add(new Imagem(nomeImagem[1], R.drawable.computador));

                        imagens.add(new Imagem(nomeImagem[2], R.drawable.camisa));

                        imagens.add(new Imagem(nomeImagem[3], R.drawable.jogo));

                        db.imagemDao().inserirLista(imagens);
                        Log.i("Insert:", "imagens");
                        List<Produto> produtos = new ArrayList<>();
                        Categoria catP1 = db.categoriaDao().getCategoriaByNome("Livros");
                        Loja lojaP1 = db.lojaDao().getLojaByNome("Saraiva");
                        Imagem imgP1 = db.imagemDao().getImagemByNome("Livro.jpg");
                        Produto p1 = new Produto("As Aventuras De Mike", 37.9, 32.2, catP1.getId(), lojaP1.getId(), imgP1.getId(),
                                -8.0676307,-34.9258136);
                        produtos.add(p1);

                        Categoria catP2 = db.categoriaDao().getCategoriaByNome("Computadores");
                        Loja lojaP2 = db.lojaDao().getLojaByNome("Nagem");
                        Imagem imgP2 = db.imagemDao().getImagemByNome("Computador.jpg");
                        Produto p2 = new Produto("Notebook Lenovo B330S Intel® Core i5-8250U 4GB 128GB SSD 14 Windows 10 Professional", 3399.00,
                                2599.00, catP2.getId(), lojaP2.getId(), imgP2.getId(),
                                -8.0382206,-34.9579281);
                        produtos.add(p2);

                        Categoria catP3 = db.categoriaDao().getCategoriaByNome("Vestuário");
                        Loja lojaP3 = db.lojaDao().getLojaByNome("C&A");
                        Imagem imgP3 = db.imagemDao().getImagemByNome("Camisa.jpg");
                        Produto p3 = new Produto("camisa jeans feminina manga longa azul claro", 99.99,
                                69.99, catP3.getId(), lojaP3.getId(), imgP3.getId(),
                                -8.0582844,-34.9509295);
                        produtos.add(p3);

                        Categoria catP4 = db.categoriaDao().getCategoriaByNome("Jogos");
                        Loja lojaP4 = db.lojaDao().getLojaByNome("Saraiva");
                        Imagem imgP4 = db.imagemDao().getImagemByNome("Jogo.jpg");
                        Produto p4 = new Produto("Marvel's Spider-Man - PS4", 169.9,
                                149.9, catP4.getId(), lojaP4.getId(), imgP4.getId(),
                                -8.0612985,-34.9486243);
                        produtos.add(p4);
                        db.produtoDao().inserirTudo(produtos);
                        Log.i("Insert:", "produtos");
                    }
                });
            }
        }).build();
    }
}
